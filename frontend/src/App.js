import React, {Component} from 'react';
import './App.css';
import {Button, Col, Container, Form, Row} from "react-bootstrap";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            message: '',
            error: '',
            handicapIndex: null,
            slopeRating: null
        };

        this.getCourseHandicap = this.getCourseHandicap.bind(this);
        this.handleHandicapChange = this.handleHandicapChange.bind(this);
        this.handleSlopeRatingChange = this.handleSlopeRatingChange.bind(this);
    }

    getCourseHandicap(event) {
        event.preventDefault();
        let handicapIndex = this.state.handicapIndex;
        let slopeRating = this.state.slopeRating;
        if (handicapIndex === null || handicapIndex === '' || slopeRating === null || slopeRating === '') {
            this.setState({message: 'Введите все поля'});
            return;
        }
        fetch('/api/courseHandicapUSGA?indexHandicap=' + handicapIndex + '&slopeRating=' + slopeRating)
            .catch(error => this.setState({message: error.message}))
            .then(response => {
                return response.text();
            })
            .then(message => {
                this.setState({message: message});
            })
    }

    handleHandicapChange(event) {
        this.setState({handicapIndex: event.target.value});
    }

    handleSlopeRatingChange(event) {
        this.setState({slopeRating: event.target.value});
    }

    render() {
        return (
            <Container>
                <Row className="justify-content-md-center">
                    <Col xs={6}>
                        <Form>
                            <Form.Group controlId="handicapIndex">
                                <Form.Label>Handicap Index</Form.Label>
                                <Form.Control type="number" placeholder="Handicap Index"
                                              onChange={this.handleHandicapChange}/>
                            </Form.Group>
                            <Form.Group controlId="slopeRating">
                                <Form.Label>Slope Rating</Form.Label>
                                <Form.Control type="number" placeholder="Slope Rating"
                                              onChange={this.handleSlopeRatingChange}/>
                            </Form.Group>
                            <Button variant="primary" onClick={this.getCourseHandicap}>Рассчитать</Button>
                            <h3>Результат: {this.state.message}</h3>
                        </Form>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default App;
