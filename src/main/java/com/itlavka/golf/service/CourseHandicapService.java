package com.itlavka.golf.service;

import com.itlavka.golf.exception.SlopeRatingOutOfRange;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class CourseHandicapService {

    /**
     * https://www.usga.org/handicapping/handicap-manual.html#!rule-14370
     * Slope Rating
     */
    private static final Integer STANDARD_SLOPE_RATING = 113;
    public static final Integer MIN_SLOPE_RATING = 55;
    public static final Integer MAX_SLOPE_RATING = 155;



    /**
     * A player's Course Handicap is determined by multiplying a Handicap Index by the
     * Slope Rating of the tees played and then dividing by 113.
     * <p>
     * https://www.usga.org/handicapping/handicap-manual.html#!rule-14389
     * section 10-4
     * @param handicapIndex A "Handicap Index" is the USGA's service mark used to indicate a measurement of a player's potential ability on a course of standard playing difficulty.
     * @param slopeRating  A "Slope Rating" is the USGA's mark that indicates the measurement of the relative difficulty of a course for players who are not scratch golfers compared to the USGA Course Rating
     * @return courseHandicapIndex A "Course Handicap" is the USGA's mark that indicates the number of handicap strokes a player receives from a specific set of tees at the course being played to adjust the player's scoring ability to the level of scratch or zero-handicap golf.
     */
    public Integer getCourseHandicap(Double handicapIndex, Integer slopeRating) throws SlopeRatingOutOfRange {
        if (!isSlopeRatingInRange(slopeRating)) {
            throw new SlopeRatingOutOfRange();
        }
        double courseHandicap = handicapIndex * slopeRating / STANDARD_SLOPE_RATING;
        BigDecimal bd = BigDecimal.valueOf(courseHandicap);
        bd = bd.setScale(0, RoundingMode.HALF_UP);
        return bd.intValue();
    }


    public boolean isSlopeRatingInRange(Integer slopeRating) {
        return slopeRating >= MIN_SLOPE_RATING && slopeRating <= MAX_SLOPE_RATING;
    }
}
