package com.itlavka.golf.controller;

import com.itlavka.golf.exception.SlopeRatingOutOfRange;
import com.itlavka.golf.service.CourseHandicapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static org.apache.commons.lang3.math.NumberUtils.*;

@Controller
@RequestMapping("/api")
public class HandicapController {

    private final CourseHandicapService courseHandicapService;

    @Autowired
    public HandicapController(CourseHandicapService courseHandicapService) {
        this.courseHandicapService = courseHandicapService;
    }

    @RequestMapping(value = "/courseHandicapUSGA", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity getCourseHandicapUSGA(
            @RequestParam("indexHandicap") String indexHandicap,
            @RequestParam("slopeRating") String slopeRatingString) throws SlopeRatingOutOfRange {
        validate(indexHandicap, slopeRatingString);
        Double courseHandicap = Double.valueOf(indexHandicap);
        Integer slopeRating = Integer.valueOf(slopeRatingString);
        if (!courseHandicapService.isSlopeRatingInRange(slopeRating)) {
            String text = "Slope Rating out of range. Min " + CourseHandicapService.MIN_SLOPE_RATING +
                    ", Max " + CourseHandicapService.MAX_SLOPE_RATING;
            return new ResponseEntity<>(text, HttpStatus.BAD_REQUEST);
        }

        Integer courseRating = courseHandicapService.getCourseHandicap(courseHandicap, slopeRating);
        return new ResponseEntity<>(courseRating, HttpStatus.OK);
    }

    private void validate(String indexHandicap, String slopeRatingString) {
        if (!isNumber(indexHandicap)) {
            throw new IllegalArgumentException("Проверьте введеный Index Handicap");
        }
        if (!isNumber(slopeRatingString)) {
            throw new IllegalArgumentException("Проверьте введеный Slope Rating");
        }
    }
}
