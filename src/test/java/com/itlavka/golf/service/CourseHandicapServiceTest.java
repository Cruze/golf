package com.itlavka.golf.service;

import com.itlavka.golf.exception.SlopeRatingOutOfRange;
import org.junit.Assert;
import org.junit.Test;

public class CourseHandicapServiceTest {


    @Test
    public void SlopeRatingMoreThanMinValue() {
        //given
        CourseHandicapService service = new CourseHandicapService();

        //when
        boolean resultFor54 = service.isSlopeRatingInRange(54);
        boolean resultFor55 = service.isSlopeRatingInRange(55);
        boolean resultFor155 = service.isSlopeRatingInRange(155);
        boolean resultFor156 = service.isSlopeRatingInRange(156);

        //then
        Assert.assertFalse(resultFor54);
        Assert.assertTrue(resultFor55);
        Assert.assertTrue(resultFor155);
        Assert.assertFalse(resultFor156);
    }

    @Test
    public void testGetCourseHandicap() throws SlopeRatingOutOfRange {
        //given
        Double handicapIndex = 1.;
        Integer slopeRating = 100;

        CourseHandicapService service = new CourseHandicapService();
        //when
        Integer result = service.getCourseHandicap(handicapIndex, slopeRating);

        //then
        Integer expected = 1;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testGetCourseHandicapRoundingUp() throws SlopeRatingOutOfRange {
        //given
        Double handicapIndex = 10.5;
        Integer slopeRating = 113;

        CourseHandicapService service = new CourseHandicapService();
        //when
        Integer result = service.getCourseHandicap(handicapIndex, slopeRating);

        //then
        Integer expected = 11;
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testGetCourseHandicapRoundingDown() throws SlopeRatingOutOfRange {
        //given
        Double handicapIndex = 10.4;
        Integer slopeRating = 113;

        CourseHandicapService service = new CourseHandicapService();
        //when
        Integer result = service.getCourseHandicap(handicapIndex, slopeRating);

        //then
        Integer expected = 10;
        Assert.assertEquals(expected, result);
    }
}